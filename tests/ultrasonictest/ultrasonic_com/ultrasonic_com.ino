// ---------------------------------------------------------------------------
// Example NewPing library sketch that does a ping about 20 times per second.
// ---------------------------------------------------------------------------

#include <NewPing.h>

/* Sonar settings */
#define TRIGGER_PIN  3  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     2  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.

  pinMode(6, OUTPUT);
  digitalWrite(6, HIGH);
}

void loop() {
  int dist;
  long duration;

  Serial.println("10 times with sonar:");
  for(int i = 0 ; i < 10000 ; i++)
  { 
    delay(50);                     // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
    dist = sonar.ping_cm();
    Serial.print("   ");
    Serial.print(dist); // Send ping, get distance in cm and print result (0 = outside set distance range)
    Serial.println(" cm");
  }
}
