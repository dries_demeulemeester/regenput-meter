# README #

Project info

Using arduino nano V3

### Arduino GUI settings ###
ATMEGA 328P (OLD bootloader)


### Used hardware? ###
IC / I2C + 1602 Blue Screen LCD Display Module for Arduino - Blue  
Improved Version Nano 3.0 Atmel Atmega328P Mini USB Board for Arduino  
Waterproof Ultrasonic Module JSN-SR04T Distance Transducer Sensor for Arduino  


### Pinnings ###

I2C on arduino: A4(SDA), A5(SCL)  
3 LEDS: PC0, PC1, PC2 - RED, ORANGE, GREEN  -  arduino pins 14, 15, 16 (A0, A1, A2)(equals D14, D15, D16)  
1 LED (3 parallel) integrated in the buttons: RED - arduino pin D11  
1 output: PB3 - LOW_LEVEL - arduino pin  D17.  
----  Can be used for e.g. pump deactivation. Pin goes high when level < 10%  
1 signal for power output to ultrasonic sensor:  arduino pin 6 (D6)
3 input buttons: PB0, PB1, PB2 - SAMPLE, REF, PAGE_SWITCH  - arduino pins 8, 9, 10 (D8, D9, D10)  


### LED calculations ###
forward current: 15mA, Vout on arduino pins: 3V3  
Voltage drops: Red = 1.7V, yellow = 2V, green = 2.1V  
Resistor vals: Red = 106Ohm, yellow = 86Ohm, green = 80Ohm  


### libraries ###
Newping --> sonar
liquidcrystal_i2c --> LCD




