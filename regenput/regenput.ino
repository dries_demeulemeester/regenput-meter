
#include <NewPing.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

/* LCD settings */
#define I2C_ADDR      (0x3F) // <<----- Add your address here.  Find it from I2C Scanner
#define BACKLIGHT_PIN (3)
#define LCD_LINE_LEN  (16)    // line length in char

LiquidCrystal_I2C  lcd(I2C_ADDR, 16, 2); // Liquid cristal setup, 16 cols, 2 rows

/* Sonar settings */
#define TRIGGER_PIN  (3)   // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     (2)   // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE (300) // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define NUM_SAMPLES  (20)  // Number of samples required for median calculation

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

/* Well configurations */
#define WELL_NECK_HEIGHT    (32) /* The distance between top of the well and the ultrasonic sensor in the neck */
#define WELL_HEIGHT         (218) /* Height of the well only (no neck) */
#define SENSOR_HEIGHT       (WELL_HEIGHT + WELL_NECK_HEIGHT) /* The distance of the sensor referred to the bottom of the well */
#define WELL_CAPACITY       (15000) /* Capacity in l */

/* Project defines */
#define DELAY_SAMPLE         (60000 * 10)  // Take sample delay in ms every 10 min
#define DELAY_2HZ_TOGGLE     (500)   // 2Hz toggle delay in ms
#define BACKLIGHT_ON_DELAY   (15000) // Backlight on time
#define SCHEDULER_DELAY      (20)    // Delay between main loop runs
#define DELAY_STORE_HISTORY  (60000 * 30) // Store current level every 30 min
#define HISTORY_SIZE         (48)    // 2 Samples/h --> 24h history
#define STARTUP_DELAY_LIGHT  (100)   // delay for startup between led tests
#define SENSOR_STARTUP_DELAY (50)   // delay for Ultrasonic sensor to be started up
#define SENSOR_DELAY_BETWEEN_SAMPLES (50) // delay between consecutive sample measurements

#define BTN_SAMPLE   (8)  /* Execute measurement */
#define BTN_SCR_PAGE (9)  /* Change screen view */
#define BTN_REF      (10) /* Set current level as a reference */

#define LED_RED     (14)  /* Red led pin */
#define LED_ORANGE  (15)  /* orange led pin */
#define LED_GREEN   (16)  /* green led pin */
#define LED_BUTTONS (11)  /* integrated button leds */
#define LOW_LVL_PIN (17)  /* Pin is set high when low level is reached (<10%). Pin could be used to switch off mains.  */
#define SENSOR_PWR  (6)   /* 3v3 signal for ultra sonic power sensor */

#define HYST_TOL           (1)  /* Hysteresis tolerance in percentage before entering next/previous region */
#define PEAK_DET_LVL       (5)  /* Number of percentage to trigger peak detection */
#define LVL_PERC(hgt)      (int((float(hgt)/float(WELL_HEIGHT)*100.0))) /* Well level in percentage */
#define LVL_IN_LITER(hgt)  (int((float(hgt) * float(WELL_CAPACITY)/(float)WELL_HEIGHT)))

#define REF_STR            ("Ref:") /* LCD Screen text for reference */
#define CUR_STR            ("Nu :") /* LCD Screen text for current */

#define INVALID_HISTORY_RECORD (99) /* The invalid value (is a value that can not be present in the history) */

// ----------------- ENUMS -------------------
enum WATERLEVELSTATE {
  E_WL_UNDEF,
  E_WL_MINUS10,
  E_WL_MINUS15,
  E_WL_MINUS30,
  E_WL_MINUS80,
  E_WL_PLUS80,
  E_WL_MAX_STATES
};

enum MAINPAGES {
  E_MP_RAW,
  E_MP_CM,
  E_MP_LTR, 
  E_MP_PERCENT,
  E_MP_HISTORY,
  E_MP_PERCENT_BARS,
  E_MP_DIAG_1,
  E_MP_DIAG_2
};

// ------------------STRUCTS--------------------
struct MEAS_ENTRY{
  int distance;
  int numHits;
};

// ------------ GLOBAL VARIABLES--------------
unsigned long gv_sched_BacklightOff = 10000;
int gv_state2Hz = LOW;

//---------------- FUNCTIONS------------------

/* Store the new distance somewhere in the array */
void handleNewMeasurement(int newDist, MEAS_ENTRY *results)
{
  int i = 0;
  /* Is sample available? */
  for(i = 0; i < NUM_SAMPLES ; i++){
    if(results[i].distance == newDist){
      /* New distance is available, increase it's counter */
      results[i].numHits++;
      break;
    }
  }
  
  /* entry not found? */
  if( i >= NUM_SAMPLES){
    /* No entry found, add it in the list on an available location*/
    i = 0;    
    while(results[i].distance != 0){
      i++;
    }
    if(i < NUM_SAMPLES){
      results[i].distance = newDist;
      results[i].numHits++;
    }
  }
}

/* Check for abnormal measurement and store peaks in array per well state */
int handleMeasurementPeaks(int currLvl, int prevLvl, WATERLEVELSTATE currentWlState, unsigned int *peakArray){
  int retval = 0;

  /* If measured percentage differs more than PEAK_DET_LVL percent, add peak error for that state, if not in init state any more */
  if((abs(LVL_PERC(currLvl) - LVL_PERC(prevLvl)) >= PEAK_DET_LVL) && (prevLvl != -1)){
    /* Increase peak error for this state */
    peakArray[currentWlState]++;
    
    /* Error detected, inform calling app */
    retval = 1;
  }
 
  return retval;
}

/* Measure depth */
int measure() {  
  int i = 0;
  int dist;
  MEAS_ENTRY meas_results[NUM_SAMPLES]; /* Reserve max number of samples, to be sure enough memory reserved in case all samples are different */
  MEAS_ENTRY highestSample = {0};
  char tmpString[50];
  
  /* Switch on sensor power */
  setSensorPower(HIGH);
  
  /* Wait for sensor to be stable */
  delay(SENSOR_STARTUP_DELAY);
  
  /* Clear memory */
  memset(meas_results, 0, sizeof(meas_results));
  
  /* Take burst of samples */
  for (i = 0; i < NUM_SAMPLES; i++)
  {
    /* Wait for sensor to be ready for next sample */
    delay(SENSOR_DELAY_BETWEEN_SAMPLES);
    
    /* Measure */
    dist = sonar.ping_cm();

    /* Store the measurement, even if it is invalid */    
    handleNewMeasurement(dist, meas_results);
  }
  
  /* Switch off sensor power */
  setSensorPower(LOW);
  
  /* Find highest hit rate sample */
  for (i = 0; i < NUM_SAMPLES; i++){
    if(meas_results[i].numHits > highestSample.numHits){
      memcpy(&highestSample, &meas_results[i], sizeof(MEAS_ENTRY));
    }
  }
  
  /* Print the distribution array */
  Serial.print("Sample result: ");
  for( i = 0 ; i < NUM_SAMPLES; i++){
    if(meas_results[i].distance == 0) break;
    sprintf(tmpString, "  %d:%d", meas_results[i].distance, meas_results[i].numHits);
    Serial.print(tmpString);
  }
  Serial.println(" <END>");

  /* Return the highest hit distance in cm */
  Serial.print("Result: ");
  Serial.print(highestSample.distance);
  Serial.println("cm");
  return highestSample.distance;
}

/* Show measurement activity on the LCD */
void showLCDActivity(int state){
  /* Set a '.' at the end of bottom line, ' ' when done */
  lcd.setCursor(LCD_LINE_LEN - 1, 1);
  
  if(state == 1){
    lcd.print('.');
  } else {
    lcd.print(' ');
  }
}

/* Print text: 
  line = line number: 1 or 2 */
void printLCD(int line, const char * text){
  static char line1[LCD_LINE_LEN + 1], line2[LCD_LINE_LEN + 1]; /* 16 chars + end */
  char *linePtr = NULL;
  
  switch(line){
    case 1:
    linePtr = line1;
    break;
    
    case 2:
    linePtr = line2;
    break;
    
    default:
    Serial.print("LCD Error: unknown line number specified: ");
    Serial.println(line);
    break;
  }
  
  if( linePtr != NULL){
    /* Copy text to line buffer (padded to clear screen) */
    snprintf(linePtr, LCD_LINE_LEN + 1, "%-16s", text);
    
    lcd.setCursor(0, line - 1);
    lcd.print(linePtr);
  }
}

/* Enable the backlight for a certain period in ms */
void enableBacklight(unsigned long millisOn)
{
  unsigned long currTime = millis();
  
  Serial.print("Backlight on for ");
  Serial.print(millisOn, DEC);
  Serial.println(" ms");
  lcd.setBacklight(HIGH);
  gv_sched_BacklightOff = currTime + millisOn;
}

bool isBacklightEnabled()
{
  return gv_sched_BacklightOff > 0;
}

/* Read the buttons states, returns HIGH when flank down has been detected (inverted logic) */
int readButton(int btnId){
  int tmpState, *prevBtnState;
  int retval = LOW;
  static int prevButLeft = HIGH, prevButMid = HIGH, prevButRight = HIGH;

  switch(btnId){
    case BTN_SAMPLE:{
      prevBtnState = &prevButLeft;
      break;
    }
    case BTN_SCR_PAGE:{
      prevBtnState = &prevButMid;
      break;
    }
    case BTN_REF:{
      prevBtnState = &prevButRight;
      break;
    }
  }
  
  /* Detect flanks of the left button */
  tmpState = digitalRead(btnId);
  if((tmpState != *prevBtnState) && (tmpState == LOW)){
    retval = HIGH;
  } 
  
  /* Store the button state for next run */
  *prevBtnState = tmpState;

  return retval;
}

/* Set led states (varable needs to be HIGH or LOW */
void setLedRed(int state){
  digitalWrite(LED_RED, state);
}
void setLedOrange(int state){
  digitalWrite(LED_ORANGE, state);
}
void setLedGreen(int state){
  digitalWrite(LED_GREEN, state);
}
void setLedButtons(int state){
  digitalWrite(LED_BUTTONS, state);
}

/* Set low level pin states (variable needs to be HIGH or LOW) */
void setLowLevelPin(int state){
  digitalWrite(LOW_LVL_PIN, state);
}

/* Set the sensour power pin */
void setSensorPower(int state){
  digitalWrite(SENSOR_PWR, state);
}

/* Handle waterlevel based on state machine */
WATERLEVELSTATE handleWaterLevelState(int lvlPercentage){
  static WATERLEVELSTATE wlState = E_WL_PLUS80;
  
  switch(wlState) {
    case E_WL_UNDEF:
      if(lvlPercentage >= 80) wlState = E_WL_PLUS80;
      if(lvlPercentage < 80) wlState = E_WL_MINUS80;
      if(lvlPercentage < 30) wlState = E_WL_MINUS30;
      if(lvlPercentage < 15) wlState = E_WL_MINUS15;
      if(lvlPercentage < 10) wlState = E_WL_MINUS10;
    break;
    
    case E_WL_PLUS80:
    /* Go to other state?*/
    if(lvlPercentage < (80 - HYST_TOL)){
      wlState = E_WL_MINUS80;
      Serial.println("state --- PLUS80 -> MINUS80");
    }
    break;
    
    case E_WL_MINUS80:
    /* Go to other state?*/
    if(lvlPercentage < (30 - HYST_TOL)){
      wlState = E_WL_MINUS30;
      Serial.println("state --- MINUS80 -> MINUS30");
    } else if (lvlPercentage > (80 + HYST_TOL)) {
      wlState = E_WL_PLUS80;
      Serial.println("state +++ MINUS80 to PLUS80");
    }
    break;
    
    case E_WL_MINUS30:
    /* Go to other state?*/
    if(lvlPercentage < (15 - HYST_TOL)){
      wlState = E_WL_MINUS15;
      Serial.println("state --- MINUS 30 -> MINUS15");
    } else if (lvlPercentage > (30 + HYST_TOL)) {
      wlState = E_WL_MINUS80;
      Serial.println("state +++ MINUS30 -> MINUS80");
    }
    break;
    
    case E_WL_MINUS15:
    /* Go to other state?*/
    if(lvlPercentage < (10 - HYST_TOL)){
      wlState = E_WL_MINUS10;
      Serial.println("state --- MINUS15 -> MINUS10");
    } else if (lvlPercentage > (15 + HYST_TOL)) {
      wlState = E_WL_MINUS30;
      Serial.println("state +++ MINUS15 -> MINUS30");
    }
    break;
    
    case E_WL_MINUS10:
    /* Go to other state?*/
    if (lvlPercentage > (10 + HYST_TOL)) {
      wlState = E_WL_MINUS15;
      Serial.println("state +++ MINUS10 -> MINUS15");
    }
    break;
  }

  return wlState;
}

/* Set the LED for diagnostics */
void handleLedDiagnostics(WATERLEVELSTATE wlState) {
  switch (wlState) {
    case E_WL_MINUS10:
    setLedRed(gv_state2Hz);
    setLedOrange(gv_state2Hz);
    setLedGreen(gv_state2Hz);
    setLowLevelPin(HIGH);
    break;
    
    case E_WL_MINUS15:
    setLedRed(HIGH);
    setLedOrange(LOW);
    setLedGreen(LOW);
    setLowLevelPin(LOW);
    break;
    
    case E_WL_MINUS30:
    setLedRed(LOW);
    setLedOrange(HIGH);
    setLedGreen(LOW);
    setLowLevelPin(LOW);
    break;
    
    case E_WL_MINUS80:
    setLedRed(LOW);
    setLedOrange(LOW);
    setLedGreen(HIGH);
    setLowLevelPin(LOW);
    break;
    
    case E_WL_PLUS80:
    setLedRed(LOW);
    setLedOrange(LOW);
    setLedGreen(HIGH);
    setLowLevelPin(LOW);
    break;
  }
}


/* Screen rendering functions */
void renderMainScreenCm(int cur, int ref){
  char textString[LCD_LINE_LEN + 1];
  
  /* Build strings and put on LCD */
  sprintf(textString, "%s %dcm", REF_STR, ref);
  printLCD(1, textString);
  
  sprintf(textString, "%s %dcm", CUR_STR, cur);
  printLCD(2, textString);
}

void renderMainScreenRaw(int raw){
  char textString[LCD_LINE_LEN + 1];

  /* Build strings and put on LCD */
  sprintf(textString, "Raw measurement:");
  printLCD(1, textString);

  sprintf(textString, "%dcm", raw);
  printLCD(2, textString);
}

void renderMainScreenLiter(int cur, int ref){
  char textString[LCD_LINE_LEN + 1];
  
  /* Build strings and put on LCD */
  sprintf(textString, "%s %d liter", REF_STR, LVL_IN_LITER(ref));
  Serial.println(textString);
  printLCD(1, textString);
  
  sprintf(textString, "%s %d liter", CUR_STR, LVL_IN_LITER(cur));
  Serial.println(textString);
  printLCD(2, textString);
}

void renderMainScreenPercent(int cur, int ref){
  char textString[LCD_LINE_LEN + 1];
  
  /* Build strings and put on LCD */
  sprintf(textString, "%s %d perc", REF_STR, LVL_PERC(ref));
  printLCD(1, textString);
  
  sprintf(textString, "%s %d perc", CUR_STR, LVL_PERC(cur));
  printLCD(2, textString);
}

void renderMainHistory(int cur, int minus6h, int minus12h, int minus24h){
  char textString[LCD_LINE_LEN + 1];
  int curPerc = LVL_PERC(cur);
  
  /* Build strings and put on LCD */
  sprintf(textString, "-24h  -12h   -6h");
  Serial.println(textString);
  printLCD(1, textString);

  /* Handle the percentages */
  if(minus24h != INVALID_HISTORY_RECORD){
    minus24h = curPerc - LVL_PERC(minus24h);
  }
  else {
    minus24h = INVALID_HISTORY_RECORD;
  }
  if(minus12h != INVALID_HISTORY_RECORD){
    minus12h = curPerc - LVL_PERC(minus12h);
  }
  else {
    minus12h = INVALID_HISTORY_RECORD;
  }
  if(minus6h != INVALID_HISTORY_RECORD){
    minus6h = curPerc - LVL_PERC(minus6h);
  }
  else {
    minus6h = INVALID_HISTORY_RECORD;
  }
  
  /* Format as "+99p  +99p  +99p" */
  sprintf(textString, "%+.2dp %+.2dp %+.2dp", minus24h, minus12h, minus6h);
  Serial.println(textString);
  printLCD(2, textString);
}

void renderMainScreenPercentageBars(int cur, int ref){
  char barStringRef[LCD_LINE_LEN + 1];
  char barStringCurr[LCD_LINE_LEN + 1];
  int i;
  
  /* Init strings */
  sprintf(barStringRef,  "Ref:");
  sprintf(barStringCurr, "Nu :");
  
  /* Fill both bars */
  for(i = 0 ; i < 100 ; i += 10){
    /* Ref level bar */
    if((i) <= LVL_PERC(ref)){
      strcat(barStringRef, "#");
    }
    
    /* Current level bar */
    if((i) <= LVL_PERC(cur)){
      strcat(barStringCurr, "#");
    }
  }
  
  /* Send to LCD */
  printLCD(1, barStringRef);
  printLCD(2, barStringCurr);

  Serial.println(barStringRef);
  Serial.println(barStringCurr);
}

void renderMainScreenDiag1(unsigned int *peakArray){
  char textString[LCD_LINE_LEN + 1];
  
  /* Build top screen string and put on LCD */
  sprintf(textString, "Err: +80 -80 -30");
  printLCD(1, textString);
  
  sprintf(textString, "     %3d %3d %3d", peakArray[E_WL_PLUS80], peakArray[E_WL_MINUS80], peakArray[E_WL_MINUS30]);
  printLCD(2, textString);
}

void renderMainScreenDiag2(unsigned int *peakArray){
  char textString[LCD_LINE_LEN + 1];
  
  /* Build top screen string and put on LCD */
  sprintf(textString, "Err: -15 -10");
  printLCD(1, textString);
  
  sprintf(textString, "     %3d %3d", peakArray[E_WL_MINUS15], peakArray[E_WL_MINUS10]);
  printLCD(2, textString);
}

/* update all ways of activity monitoring */
void showActivity(int state){
  /* Trigger LCD activity */
  showLCDActivity(state);
  
  /* Trigger button LED activity */
  setLedButtons(state);
}
//---------------MAIN FUNCTIONS-------------------

void setup() {
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
  while (!Serial);      // Leonardo: wait for serial monitor

  Serial.println("Init...");
  /* Init LCD */
  lcd.init();
  
  printLCD(1, "Regenput meter");
  printLCD(2, "Opstarten...");
  
  /* Init buttons as input and pull-up */
  pinMode(BTN_SAMPLE, INPUT_PULLUP);
  pinMode(BTN_REF, INPUT_PULLUP);
  pinMode(BTN_SCR_PAGE, INPUT_PULLUP);

  /* Init the sensor power as an output pin and low state (power off) */
  pinMode(SENSOR_PWR, OUTPUT);
  setSensorPower(LOW);
  
  /* Init LEDs as output */
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_ORANGE, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_BUTTONS, OUTPUT);

  
  /* Init low level pin */
  pinMode(LOW_LVL_PIN, OUTPUT);
  setLowLevelPin(LOW);
  

  setLedRed(HIGH);
  delay(STARTUP_DELAY_LIGHT);
  setLedOrange(HIGH);
  delay(STARTUP_DELAY_LIGHT);
  setLedGreen(HIGH);
  delay(STARTUP_DELAY_LIGHT);
  setLedButtons(HIGH);
  delay(STARTUP_DELAY_LIGHT);
  enableBacklight(BACKLIGHT_ON_DELAY);
  delay(STARTUP_DELAY_LIGHT);
  setLedRed(LOW);
  delay(STARTUP_DELAY_LIGHT);
  setLedOrange(LOW);
  delay(STARTUP_DELAY_LIGHT);
  setLedGreen(LOW);
  delay(STARTUP_DELAY_LIGHT);
  setLedButtons(LOW);
  delay(STARTUP_DELAY_LIGHT);
  printLCD(2, "Klaar!");
  Serial.println("Done");
}

/* Main loop */
void loop() {
  unsigned long currTime;
  unsigned long sched_TakeSample = 0;
  unsigned long sched_2HzToggle = DELAY_2HZ_TOGGLE;
  unsigned long sched_StoreCurrHistory = DELAY_STORE_HISTORY;
  int triggerMainScreenRender = 0;
  char textString[LCD_LINE_LEN + 1];
  
  unsigned int measurementPeaks[E_WL_MAX_STATES] = {0};
  int levelHistory[HISTORY_SIZE] = {0};
  int currWaterLevel = 0, prevWaterLevel = -1,  ref = 0, rawMeasurement = 0;
  WATERLEVELSTATE wlState;
  MAINPAGES mainPageSelected = E_MP_RAW;

  /* Init history */
  for(int i = 0; i < HISTORY_SIZE ; i++){
    levelHistory[i] = INVALID_HISTORY_RECORD;
  }

  while(1){
    /* Store current time */
    currTime = millis();
    
    /* Check button actions, no timing needed, buttons can be checked at each run */
    if(readButton(BTN_SAMPLE) == HIGH){
      if(isBacklightEnabled()){
        Serial.println("SAMPLE press");
        /* Execute measurement */
        sched_TakeSample = 0;  /* Trick to trigger measurement */
      }
      enableBacklight(BACKLIGHT_ON_DELAY);
    }
    
    /* Handle reference setting */
    if(readButton(BTN_REF) == HIGH){
      if(isBacklightEnabled()){
        Serial.println("REF press");
        /* Store current level as ref */
        ref = currWaterLevel;
        
        /* Inform user with ack */
        sprintf(textString, "Huidig niveau");
        printLCD(1, textString);
        sprintf(textString, "opgeslagen!");
        printLCD(2, textString);
        
        /* Wait 1.5 sec */
        delay(500);
        
        /* Build screen */
        triggerMainScreenRender = 1;
      }
      /* Enable backlight */
      enableBacklight(BACKLIGHT_ON_DELAY);
    }
    
    /* Handle main page selection */
    if(readButton(BTN_SCR_PAGE) == HIGH){
      if(isBacklightEnabled()){
        Serial.print("Page swap");
        switch(mainPageSelected){
          case E_MP_RAW:
            /* Set next render page */
            mainPageSelected = E_MP_CM;
            break;
            
          case E_MP_CM:
            /* Set next render page */
            mainPageSelected = E_MP_LTR;
            break;
          
          case E_MP_LTR:
            /* Set next render page */
            mainPageSelected = E_MP_PERCENT;
            break;
            
          case E_MP_PERCENT:
            /* Set next render page */
            mainPageSelected = E_MP_PERCENT_BARS;
            break;
            
          case E_MP_PERCENT_BARS:
            /* Set next render page */
            mainPageSelected = E_MP_HISTORY;
            break;
            
          case E_MP_HISTORY:
            /* Set next render page */
            mainPageSelected = E_MP_DIAG_1;
            break; 
            
          case E_MP_DIAG_1:
            /* Set next render page */
            mainPageSelected = E_MP_DIAG_2;
            break;
            
          case E_MP_DIAG_2:
            /* Set next render page */
            mainPageSelected = E_MP_RAW;
            break;
        }
        
        /* Prepare for screen render */
        triggerMainScreenRender = 1;
      }
      /* Enable backlight */
      enableBacklight(BACKLIGHT_ON_DELAY);
    }
    
    /* Set Led diagnostics, no timing needed, leds can be updated at each run */
    handleLedDiagnostics(wlState);
    
    /* Take sample */
    if(currTime > sched_TakeSample){
      static unsigned long lastSched = 0;
      static int overflowDetected = 0;
      
      if(overflowDetected == 1){
        Serial.println("Scheduler overflow detected, ignore run for sample schedule");
        if(currTime < DELAY_SAMPLE){
          overflowDetected = 0;
        }
      } else {
        Serial.println("Start measurement");  

        /* Show measurement activity */
        showActivity(1);
        
        /* Measure */
        rawMeasurement = measure();
        currWaterLevel = SENSOR_HEIGHT - rawMeasurement;
        Serial.print("Level is at ");
        Serial.print(currWaterLevel, DEC);
        Serial.println(" cm");
        
        /* Correct value when negative */
        if(currWaterLevel < 0){
          Serial.println("Error! Corrected negative measurement to 0");
          currWaterLevel = 0;
        }
    
        /* End measurement activity */
        showActivity(0);
        
        /* decide on which level state the water is */
        wlState = handleWaterLevelState(LVL_PERC(currWaterLevel));
        
        /* Store measurements diagnostics */
        if(handleMeasurementPeaks(currWaterLevel, prevWaterLevel, wlState, measurementPeaks) != 0){
          Serial.println("Measurement peak detected");
        }

        /* Store new valid measurement value */
        prevWaterLevel = currWaterLevel;
        
        /* Build screen */
        triggerMainScreenRender = 1;
        
        /* Schedule next run */
        sched_TakeSample = currTime + DELAY_SAMPLE; 
        
        if(sched_TakeSample < lastSched){
          /* Overflow detected */
          overflowDetected = 1;
        }
      }
      
      lastSched = sched_TakeSample;
    }
    
    /* Handle main screen page rendering */
    if(triggerMainScreenRender == 1){
      /* First, clear the boolean */
      triggerMainScreenRender = 0;
      switch(mainPageSelected){
        case E_MP_RAW:
          renderMainScreenRaw(rawMeasurement);
          break;
          
        case E_MP_CM:
          renderMainScreenCm(currWaterLevel, ref);
          break;
        
        case E_MP_LTR:
          renderMainScreenLiter(currWaterLevel, ref);
          break;
          
        case E_MP_PERCENT:
          renderMainScreenPercent(currWaterLevel, ref);
          break;
          
        case E_MP_HISTORY:
          renderMainHistory(currWaterLevel, levelHistory[11], levelHistory[23], levelHistory[47]);
          break; 
          
        case E_MP_PERCENT_BARS:
          renderMainScreenPercentageBars(currWaterLevel, ref);
          break;
          
        case E_MP_DIAG_1:
          renderMainScreenDiag1(measurementPeaks);
          break;
          
        case E_MP_DIAG_2:
          renderMainScreenDiag2(measurementPeaks);
          break;
      }
    }
    
    /* Store the history */
    if(currTime > sched_StoreCurrHistory){
      static unsigned long lastSched = 0;
      static int overflowDetected = 0;
      int i;

      if(overflowDetected == 1){
        Serial.println("Scheduler overflow detected for store of history");
        if(currTime < DELAY_STORE_HISTORY){
          overflowDetected = 0;
        }
      } else {
        Serial.println("Store newest sample in history");
        
        /* Print current memory */
        Serial.print("Before: ");
        for(i = 1; i <= HISTORY_SIZE ; i++){
          Serial.print("-");
          Serial.print(levelHistory[HISTORY_SIZE - i]);
        }
        Serial.println();
        /* First, Shift history fifo */
        for(i = 1; i <= HISTORY_SIZE - 1; i++){
          levelHistory[HISTORY_SIZE - i] = levelHistory[HISTORY_SIZE - i - 1];
        }
                
        /* Now, store the current level value */
        levelHistory[0] = currWaterLevel;
        
        /* Print new state */
        Serial.print("After: ");
        for(i = 1; i <= HISTORY_SIZE ; i++){
          Serial.print("-");
          Serial.print(levelHistory[HISTORY_SIZE - i]);
        }
        Serial.println();
        
        /* Schedule next run */
        sched_StoreCurrHistory = currTime + DELAY_STORE_HISTORY;
        
        if(sched_StoreCurrHistory < lastSched){
          /* Overflow detected */
          overflowDetected = 1;
        }
      }
      
      lastSched = sched_StoreCurrHistory;
    }
    
    /* Handle backlight off */
    if((gv_sched_BacklightOff != 0) && (currTime > gv_sched_BacklightOff)){
      Serial.println("Switch backlight off");
      /* Switch off backlight */
      lcd.setBacklight(LOW);
      /* Set to 0 to prevent continues execution */
      gv_sched_BacklightOff = 0;
    }
    
    /* 2HZ toggle generator */
    if(currTime > sched_2HzToggle){
      /* Toggle the variable */
      if(gv_state2Hz == LOW) {
        gv_state2Hz = HIGH;
      } else {
        gv_state2Hz = LOW;
      }
      
      /* Schedule next run */
      sched_2HzToggle = currTime + DELAY_2HZ_TOGGLE;
    }
    delay(SCHEDULER_DELAY);
  }
}
